package fit5042.tutex;

import java.util.List;
import java.util.Scanner;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial
 * instructions. This is the main driver class. This class contains the main
 * method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
	private String name;
	private final PropertyRepository propertyRepository;

	public RealEstateAgency(String name) throws Exception {
		this.name = name;
		this.propertyRepository = PropertyRepositoryFactory.getInstance();
	}

	// this method is for initializing the property objects
	// complete this method
	public void createProperty() {

		Property p1 = new Property(10, "Boston Avenue ,Malvern East 3145", 2, 5, 420.5);
		Property p2 = new Property(222, "Bettina St", 3, 5, 420.5);
		Property p3 = new Property(13, "Wattle Avenue", 4, 6, 780.5);
		Property p4 = new Property(45, "hamilton St.", 2, 4, 890.5);
		Property p5 = new Property(57, "Spring Rd.", 6, 5, 400.5);

		try {
			propertyRepository.addProperty(p1);
			propertyRepository.addProperty(p2);
			propertyRepository.addProperty(p3);
			propertyRepository.addProperty(p4);
			propertyRepository.addProperty(p5);
			System.out.println("5 properties added successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// this method is for displaying all the properties
	// complete this method
	public void displayProperties() {

		try {
			List<Property> properties = propertyRepository.getAllProperties();
			int counter = 0;
			for (Property property : properties) {
				counter++;
				System.out.println(counter + " " + property.toString());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// this method is for searching the property by ID
	// complete this method
	public void searchPropertyById() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the property of Id to be searched..");
		Integer inputId = sc.nextInt();
		try {
			Property property = propertyRepository.searchPropertyById(inputId);
			System.out.println(property.toString());
		} catch (Exception e) {
			System.out.println("Id not present");
		}

	}

	public void run() {
		createProperty();
		System.out.println("********************************************************************************");
		displayProperties();
		System.out.println("********************************************************************************");
		searchPropertyById();
	}

	public static void main(String[] args) {
		try {
			new RealEstateAgency("FIT5042 Real Estate Agency").run();
		} catch (Exception ex) {
			System.out.println("Application fail to run: " + ex.getMessage());
		}
	}
}
